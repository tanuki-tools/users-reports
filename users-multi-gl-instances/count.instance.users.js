const GLClient = require("../libs/gl-cli").GLClient

async function getAllUsers(gitLabConnection, {perPage, page}) {
  return gitLabConnection.getAllUsers({perPage, page})
}

async function getInstanceUsers(gitLabConnection) {
  console.log(">> get users of the instance")
  try {
    var stop = false, page = 1, users = []

    while (!stop) {
      await getAllUsers(gitLabConnection, {perPage:10, page:page}).then(someUsers => {
        if(someUsers.length == 0) { stop = true }
        users = users.concat(someUsers.map(user => {
          return { 
              username: user.username
            , web_url: user.web_url
            , state: user.state
            , email: user.email 
            , is_admin: user.is_admin
          }
        }))
      })
      page +=1
    }
    console.log(">> users ready")
    return users
  } catch (error) {
    console.log("😡:", error)
  }
}

async function batch(gitLabConnection) {
  return await getInstanceUsers(gitLabConnection)
}

let gitLab = new GLClient({
  baseUri: `${process.env.URL}/api/v4`,
  token: process.env.TOKEN
})

batch(gitLab).then(results => {
  let gitLabInstanceName = process.argv[2]
  let activeUsers = results.filter(user => user.state !== "blocked")
  let blockedUsers = results.filter(user => user.state == "blocked")
  let activeAdmins = activeUsers.filter(user => user.is_admin)
  
  console.log(`===== ${gitLabInstanceName} =====`)
  console.log(`paid users (including ${activeAdmins.length} admins): ${activeUsers.length}`)
  console.log(`blocked users: ${blockedUsers.length}`)
  
}).catch(error => {
  console.log("😡:", error)
})


